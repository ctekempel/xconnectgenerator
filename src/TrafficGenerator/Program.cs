﻿using System;
using System.Collections.Generic;
using Sitecore.XConnect;
using TrafficGenerator.Content;
using TrafficGenerator.Content.Model;
using TrafficGenerator.Generation;
using TrafficGenerator.Generation.Model;

namespace TrafficGenerator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var configuration = new Configuration();
            var generationService = new GenerationService(configuration);

            var generationModel = GetGenerationModel(configuration);

            foreach (var statusMessage in generationService.RunGeneration(generationModel))
                Console.WriteLine(statusMessage);

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static GenerationModel GetGenerationModel(Configuration configuration)
        {
            return new GenerationModel
            {
                StartDate = new DateTime(2018, 02, 1),
                EndDate = new DateTime(2018, 02, 28),
                Audience = new AudienceModel
                {
                    PercentageNewContacts = 80,
                    PercentageKnownContacts = 15,
                    Locations = new List<Location>{
                                                     new Location("AU","02","Sydney","139.99.172.152"),
                                                     new Location("AU","05","Adelaide", "42.241.166.150")
                    },
                    UserAgents = new List<string>
                    {
                        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.6.01001)",
                        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.7.01001)",
                        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.5.01003)",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0",
                        "Mozilla/5.0 (X11; U; Linux x86_64; de; rv:1.9.2.8) Gecko/20100723 Ubuntu/10.04 (lucid) Firefox/3.6.8",
                        "Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:11.0) Gecko/20100101 Firefox/11.0",
                        "Mozilla/5.0 (X11; U; Linux x86_64; de; rv:1.9.2.8) Gecko/20100723 Ubuntu/10.04 (lucid) Firefox/3.6.8",
                        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.0.3705)",
                        "Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1"
                    },
                },
                Content = ContentService.LoadContentItems(configuration),
                Media = ContentService.LoadMediaItems(configuration),
                Events = ContentService.LoadGoalsAndEventsItems(configuration),
                Traffic = new TrafficModel
                {
                    InteractionsPerDay = 50,
                    SiteName = "website",
                    ItemRoot = "/sitecore/content",
                    MediaRoot = "/sitecore/media library",
                    KnownPaths = new List<KnownPathModel>
                    {
                        new KnownPathModel(.25, "en", Channels.Default, new ContentImpression("/Home")),
                        new KnownPathModel(.25, "en", Channels.Default,
                            new ContentImpression("/Home"),
                            new ContentImpression("/Home/Category 1"),
                            new ContentImpression("/Home/Category 1/Product 1")),
                        new KnownPathModel(.25, "en", Channels.Default,
                            new ContentImpression("/Home"),
                            new ContentImpression("/Home/Category 2")),
                        new KnownPathModel(.1, "en", Channels.AppsOtherApps,
                            new ContentImpression("/Home"),
                            new ContentImpression("/Home/Register"))
                        {
                            CampaignId = new Guid("{EEA7DC9C-073C-4847-EDD3-5A2DC8FD6DDA}")
                        },
                        new KnownPathModel(.05, "en", Channels.AppsOtherApps,
                            new ContentImpression("/Home"),
                            new ContentImpression("/Home/Register")) {Referrer = "https://google.com"},
                        new KnownPathModel(.05, "en", Channels.AppsOtherApps,
                            new ContentImpression("/Home"),
                            //new MediaImpression("/Files/Privacy Policy"),
                            new ContentImpression("/Home/Register", new OutcomeImpression(new Guid("{5646D20E-B10A-42BA-876B-2A3BB3CBC641}"),"USD",100 )))
                        {
                            Referrer = "https://google.com"
                        },
                        new KnownPathModel(.05, "en", Channels.Default,
                            new ContentImpression("/Home"),
                            new ContentImpression("/Home/Search",searchPhrase: "some search")) ,
                    },
                    DayBias = new DayBias
                    {
                        Monday = .8,
                        Tuesday = .85,
                        Wednesday = .9,
                        Thursday = .95,
                        Friday = 1,
                        Saturday = .85,
                        Sunday = .75
                    },
                    MonthBias = new MonthBias
                    {
                        January = .95,
                        February = .9,
                        March = .8,
                        April = .8,
                        May = .85,
                        June = .9,
                        July = .85,
                        August = .8,
                        September = .8,
                        October = .85,
                        November = .9,
                        December = 1
                    }
                }
            };
        }
    }
}