﻿using System.Collections.Generic;

namespace TrafficGenerator.Content.Model
{
    public class Tracking
    {
        public Tracking()
        {
            this.Events = new List<TrackingEvent>();
            this.Campaigns = new List<Campaign>();
        }

        public List<TrackingEvent> Events { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public List<Profile> Profiles { get; set; }
    }
}