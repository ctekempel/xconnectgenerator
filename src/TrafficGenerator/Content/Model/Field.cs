﻿using System;

namespace TrafficGenerator.Content.Model
{
    public class Field
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public Guid Id { get; set; }
    }
}