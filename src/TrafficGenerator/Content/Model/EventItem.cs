﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace TrafficGenerator.Content.Model
{
    public class EventItem : SitecoreItem
    {
        public int EngagementValue
        {
            get
            {
                var pointsField = this.Fields.FirstOrDefault(field => field.Name == "Points");
                if (pointsField != null && !string.IsNullOrEmpty(pointsField.Value))
                {
                    return int.Parse(pointsField.Value);
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}