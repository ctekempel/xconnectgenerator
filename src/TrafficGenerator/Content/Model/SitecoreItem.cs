﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Runtime.Remoting.Services;
using System.Xml;
using System.Xml.Serialization;

namespace TrafficGenerator.Content.Model
{
    public abstract class SitecoreItem
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Path { get; set; }
        public string Language { get; set; }
        public int Version { get; set; }
        public bool IsLatestVersion { get; set; }
        public Guid TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string Created { get; set; }
        public string Updated { get; set; }
        public Guid ParentId { get; set; }
        public Guid Id { get; set; }
        public Uri Url { get; set; }

        public List<Field> StandardFields { get; set; }
        public List<Field> Fields { get; set; }
    }
}