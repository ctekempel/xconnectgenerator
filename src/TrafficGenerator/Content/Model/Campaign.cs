﻿using System;

namespace TrafficGenerator.Content.Model
{
    public class Campaign
    {
        public string Title { get; set; }
        public Guid Id { get; set; }
    }
}