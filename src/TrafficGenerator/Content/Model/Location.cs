﻿namespace TrafficGenerator.Content.Model
{
    /// <summary>
    /// References:
    /// Country Codes - https://dev.maxmind.com/geoip/legacy/codes/iso3166/
    /// Region Codes - https://www.maxmind.com/download/geoip/misc/region_codes.csv
    /// </summary>
    public class Location
    {
        public string Country { get; }
        public string Region { get; }
        public string City { get; }
        public string IpAddress { get;  }

        public Location(string country, string region, string city, string ipAddress)
        {
            Country = country;
            Region = region;
            City = city;
            IpAddress = ipAddress;
        }
    }
}