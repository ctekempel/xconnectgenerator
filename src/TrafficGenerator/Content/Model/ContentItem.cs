﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.ModelBinding;
using System.Xml.Linq;
using System.Xml.XPath;

namespace TrafficGenerator.Content.Model
{
    public class ContentItem : SitecoreItem
    {
        public Tracking Tracking
        {
            get
            {
                var tracking = new Tracking();

                var trackingField = this.StandardFields.FirstOrDefault(field => field.Name == "__Tracking");
                if (trackingField != null && !string.IsNullOrEmpty(trackingField.Value))
                {
                    var doc = XDocument.Load(new StringReader(trackingField.Value));

                    var events = doc.Root.XPathSelectElements("/tracking/event");
                    tracking.Events = events.Select(element => new TrackingEvent {Id = Guid.Parse(element.Attribute("id").Value), Name = element.Attribute("name").Value})
                            .ToList();

                    var campaigns = doc.Root.XPathSelectElements("/tracking/campaign");
                    tracking.Campaigns = campaigns.Select(element => new Campaign{ Id = Guid.Parse(element.Attribute("id").Value), Title = element.Attribute("title").Value })
                        .ToList();

                    var profiles = doc.Root.XPathSelectElements("/tracking/profile");
                    tracking.Profiles = profiles.Select(profileElement => new Profile
                    {
                        Id = Guid.Parse(profileElement.Attribute("id").Value),
                        Name = profileElement.Attribute("name").Value,
                        Keys = profileElement.XPathSelectElements("./key").Select(profileKeyElement => new ProfileKey{ Name= profileKeyElement.Attribute("name").Value, Value = int.Parse(profileKeyElement.Attribute("value").Value) }).ToList()
                    }).ToList();
                }

                return tracking;
            }
        }

    }

    public class ProfileKey
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

    public class Profile
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ProfileKey> Keys { get; set; }
    }
}