﻿using System.Collections.Generic;

namespace TrafficGenerator.Content.Model
{
    public class ODataEnumerable<T>
    {
        public IEnumerable<T> Value { get; set; }
    }
}