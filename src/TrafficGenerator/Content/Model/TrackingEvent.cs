﻿using System;

namespace TrafficGenerator.Content.Model
{
    public class TrackingEvent
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}