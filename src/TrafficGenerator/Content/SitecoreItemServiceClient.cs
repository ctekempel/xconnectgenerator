﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using TrafficGenerator.Content.Model;

namespace TrafficGenerator.Content
{
    public class SitecoreItemServiceClient
    {
        private readonly Uri _uri;
        private readonly string _apiKey;

        public SitecoreItemServiceClient(Uri uri, string apiKey)
        {
            _uri = uri;
            _apiKey = apiKey;
        }

        public IEnumerable<T> GetDescendants<T>(string itemPath) where T:SitecoreItem
        {
            var sitecoreItems = new List<T>();
            var queue = new List<string> {itemPath};

            while (queue.Any())
            {
                var queueItem = queue[0];
                IEnumerable<T> children = GetChildren<T>(queueItem);
                sitecoreItems.AddRange(children);
                queue.AddRange(children.Select(item => item.Path));
                queue.RemoveAt(0);
            }

            return sitecoreItems;
        }

        private IEnumerable<T> GetChildren<T>(string itemPath) where T : SitecoreItem
        {
            using (var webClient = new WebClient())
            {
                string url = $"{_uri}Items('{itemPath.Replace(" ","%20")}')/Children?$expand=Fields,StandardFields&sc_apikey={_apiKey}";
                var result = webClient.DownloadString(url);
                var items = JsonConvert.DeserializeObject<ODataEnumerable<T>>(result);
                return items.Value;
            }
        }
    }
}