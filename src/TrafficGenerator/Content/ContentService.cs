﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrafficGenerator.Content.Model;

namespace TrafficGenerator.Content
{
    public class ContentService
    {
        public static Dictionary<string, ContentItem> LoadContentItems(Configuration configuration)
        {
            var client = new SitecoreItemServiceClient(configuration.OdataItemService, configuration.OdataItemServiceApiKey);

            var paths = new[] { "/sitecore/content" };
            var items = paths.SelectMany(path => client.GetDescendants<ContentItem>(path)).ToDictionary(item => item.Path, item => item);

            return items;
        }

        public static Dictionary<string, ContentItem> LoadMediaItems(Configuration configuration)
        {
            var client = new SitecoreItemServiceClient(configuration.OdataItemService, configuration.OdataItemServiceApiKey);

            var paths = new[] { "/sitecore/media library" };
            var items = paths.SelectMany(path => client.GetDescendants<ContentItem>(path)).ToDictionary(item => item.Path, item => item);

            return items;
        }

        public static Dictionary<Guid, EventItem> LoadGoalsAndEventsItems(Configuration configuration)
        {
            var client = new SitecoreItemServiceClient(configuration.OdataItemService, configuration.OdataItemServiceApiKey);

            var paths = new[] { "/sitecore/system/Marketing Control Panel/Goals", "/sitecore/system/Settings/Analytics/Page Events" };
            var items = paths.SelectMany(path => client.GetDescendants<EventItem>(path)).ToDictionary(item => item.Id, item => item);

            return items;
        }
    }
}