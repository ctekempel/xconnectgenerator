﻿namespace TrafficGenerator
{
    using System;
    using System.Configuration;

        public class Configuration
        {
            public Uri XConnectCollectionEndPoint => new Uri(ConfigurationManager.ConnectionStrings["xconnect.collection"].ConnectionString);
            public Uri XConnectSearchEndPoint => new Uri(ConfigurationManager.ConnectionStrings["xconnect.collection"].ConnectionString);
            public Uri XConnectConfigurationEndPoint => new Uri(ConfigurationManager.ConnectionStrings["xconnect.configuration"].ConnectionString);
            public string XConnectCertificateName => ConfigurationManager.ConnectionStrings["xconnect.collection.certificate"].ConnectionString;
            public string XConnectTimeout => ConfigurationManager.AppSettings["xConnectTimeout"];
            public int BatchSize => int.Parse(ConfigurationManager.AppSettings["batchSize"]);
            public string OdataItemServiceApiKey => ConfigurationManager.ConnectionStrings["OdataItemServiceApiKey"].ConnectionString;
            public Uri OdataItemService => new Uri(ConfigurationManager.ConnectionStrings["OdataItemService"].ConnectionString);
        }
}