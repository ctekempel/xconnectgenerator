﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TrafficGenerator
{
    public static class RandomExtensions
    {
        private static readonly Random Random = new Random((int)DateTime.Now.Ticks);
        public static T PickRandom<T>(this IList<T> enumerable)
        {
            var index = Random.Next(0, enumerable.Count() - 1);
            return enumerable[index];
         }
    }
}