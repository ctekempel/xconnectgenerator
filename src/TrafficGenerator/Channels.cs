﻿using System;

namespace TrafficGenerator
{
    public static class Channels
    {
        public static Guid Default = new Guid("{B418E4F2-1013-4B42-A053-B6D4DCA988BF}");
        public static Guid AppsOtherApps = new Guid("{59BD107F-D725-4BA1-91C6-61BEE3CB768C}");
    }
}