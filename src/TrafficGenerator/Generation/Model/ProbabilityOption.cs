﻿namespace TrafficGenerator.Generation.Model
{
    public class ProbabilityOption<T>
    {
        public T Value { get; set; }

        /// <summary>
        /// Percentage liklihood of it occuring, e.g. 0.5 for 50% up to 3 decimal places
        /// </summary>
        public double Probability { get; set; }
    }
}