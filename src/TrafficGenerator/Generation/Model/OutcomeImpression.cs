﻿using System;

namespace TrafficGenerator.Generation.Model
{
    public class OutcomeImpression
    {
        public OutcomeImpression(Guid outcomeId, string currencyCode, decimal monetaryValue)
        {
            OutcomeId = outcomeId;
            CurrencyCode = currencyCode;
            MonetaryValue = monetaryValue;
        }

        public Guid OutcomeId { get; }
        public string CurrencyCode { get; }
        public decimal MonetaryValue { get; }
    }
}