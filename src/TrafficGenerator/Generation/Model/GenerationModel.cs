﻿using System;
using System.Collections.Generic;
using TrafficGenerator.Content.Model;

namespace TrafficGenerator.Generation.Model
{
    public class GenerationModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AudienceModel Audience { get; set; }
        public TrafficModel Traffic { get; set; }
        public Dictionary<string, ContentItem> Content { get; set; }
        public Dictionary<string, ContentItem> Media { get; set; }
        public Dictionary<Guid, EventItem> Events { get; set; }
    }
}