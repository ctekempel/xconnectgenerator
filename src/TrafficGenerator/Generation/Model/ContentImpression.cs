﻿using Sitecore.XConnect;

namespace TrafficGenerator.Generation.Model
{
    class ContentImpression : IItemImpression
    {
        public ContentImpression(string path, OutcomeImpression outcome = null, string searchPhrase = null)
        {
            Path = path;
            Outcome = outcome;
            SearchPhrase = searchPhrase;
        }

        public string Path { get; }
        public OutcomeImpression Outcome { get; }
        public string SearchPhrase { get; }
    }
}