﻿using System;
using System.Collections.Generic;

namespace TrafficGenerator.Generation.Model
{
    public class KnownPathModel
    {
        public KnownPathModel(double percentageOfTraffic, string language, Guid channelId, params IItemImpression[] path)
        {
            PercentageOfTraffic = percentageOfTraffic;
            Language = language;
            Path = path;
            ChannelId = channelId;
        }

        public Guid ChannelId { get; }

        public Guid? CampaignId { get; set; }
        public string Referrer { get; set; }

        public double PercentageOfTraffic { get; }
        public string Language { get; }
        public IEnumerable<IItemImpression> Path { get; }
    }
}