﻿namespace TrafficGenerator.Generation.Model
{
    public interface IItemImpression
    {
        string Path { get; }
        OutcomeImpression Outcome { get; }
        string SearchPhrase { get; }
    }
}