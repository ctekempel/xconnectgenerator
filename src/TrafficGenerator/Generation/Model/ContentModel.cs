﻿using System;

namespace TrafficGenerator.Generation.Model
{
    public class ContentModel
    {
        public ContentModel(string url, Guid id, int contentVersion, string language)
        {
            Id = id;
            ContentVersion = contentVersion;
            Language = language;
            Url = url;
        }


        public Guid Id { get; }
        public int ContentVersion { get; }
        public string Language { get; }
        public string Url { get; }
    }
}