﻿using System;
using System.Collections.Generic;

namespace TrafficGenerator.Generation.Model
{
    public class DayBias
    {
        private readonly Dictionary<DayOfWeek, double> _biases;

        public DayBias()
        {
            _biases = new Dictionary<DayOfWeek, double>();
        }


        public double Monday
        {
            get => this._biases[DayOfWeek.Monday];
            set => this._biases[DayOfWeek.Monday] = value;
        }
        public double Tuesday
        {
            get => this._biases[DayOfWeek.Tuesday];
            set => this._biases[DayOfWeek.Tuesday] = value;
        }
        public double Wednesday
        {
            get => this._biases[DayOfWeek.Wednesday];
            set => this._biases[DayOfWeek.Wednesday] = value;
        }
        public double Thursday
        {
            get => this._biases[DayOfWeek.Thursday];
            set => this._biases[DayOfWeek.Thursday] = value;
        }
        public double Friday
        {
            get => this._biases[DayOfWeek.Friday];
            set => this._biases[DayOfWeek.Friday] = value;
        }
        public double Saturday
        {
            get => this._biases[DayOfWeek.Saturday];
            set => this._biases[DayOfWeek.Saturday] = value;
        }
        public double Sunday
        {
            get => this._biases[DayOfWeek.Sunday];
            set => this._biases[DayOfWeek.Sunday] = value;
        }

        public double GetBias(DayOfWeek dayOfWeek)
        {
            return this._biases[dayOfWeek];
        }
    }
}