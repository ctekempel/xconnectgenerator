﻿using System.Collections.Generic;

namespace TrafficGenerator.Generation.Model
{
    public class TrafficModel
    {
        public int InteractionsPerDay { get; set; }
        public List<KnownPathModel> KnownPaths { get; set; }
        public string SiteName { get; set; }
        public DayBias DayBias { get; set; }
        public MonthBias MonthBias { get; set; }
        /// <summary>
        ///  used to load meta data about content
        /// </summary>
        public string ItemRoot { get; set; }
        /// <summary>
        ///  used to load meta data about media
        /// </summary>
        public string MediaRoot { get; set; }
    }
}