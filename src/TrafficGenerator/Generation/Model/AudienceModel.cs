﻿using System;
using System.Collections.Generic;
using TrafficGenerator.Content.Model;

namespace TrafficGenerator.Generation.Model
{
    public class AudienceModel
    {
        public double PercentageNewContacts { get; set; }
        public double PercentageKnownContacts { get; set; }
        public IList<Location> Locations { get; set; }
        public IList<string> UserAgents { get; set; }
    }
}