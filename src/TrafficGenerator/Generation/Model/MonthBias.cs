﻿using System.Collections.Generic;

namespace TrafficGenerator.Generation.Model
{
    public class MonthBias
    {
        private readonly Dictionary<int,double> _biases;

        public MonthBias()
        {
            _biases = new Dictionary<int, double>();
        }

        public double January
        {
            get => this._biases[1];
            set => this._biases[1] = value;
        }
        public double February         {
            get => this._biases[2];
            set => this._biases[2] = value;
        }
        public double March
        {
            get => this._biases[3];
            set => this._biases[3] = value;
        }
        public double April
        {
            get => this._biases[4];
            set => this._biases[4] = value;
        }
        public double May
        {
            get => this._biases[5];
            set => this._biases[5] = value;
        }
        public double June
        {
            get => this._biases[6];
            set => this._biases[6] = value;
        }
        public double July
        {
            get => this._biases[7];
            set => this._biases[7] = value;
        }
        public double August
        {
            get => this._biases[8];
            set => this._biases[8] = value;
        }
        public double September
        {
            get => this._biases[9];
            set => this._biases[9] = value;
        }
        public double October
        {
            get => this._biases[10];
            set => this._biases[10] = value;
        }
        public double November
        {
            get => this._biases[11];
            set => this._biases[11] = value;
        }
        public double December
        {
            get => this._biases[12];
            set => this._biases[12] = value;
        }

        public double GetBias(int monthOfYear)
        {
            return this._biases[monthOfYear];
        }
    }
}