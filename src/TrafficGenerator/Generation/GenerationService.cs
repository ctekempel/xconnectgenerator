﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Client.WebApi;
using Sitecore.XConnect.Collection.Model;
using Sitecore.XConnect.Schema;
using Sitecore.Xdb.Common.Web;
using TrafficGenerator.Generation.Model;

namespace TrafficGenerator.Generation
{
    public class GenerationService
    {
        private readonly Configuration _configuration;

        public GenerationService(Configuration configuration)
        {
            _configuration = configuration;
        }

        public IEnumerable<string> RunGeneration(GenerationModel model)
        {
            yield return "Starting";
            var xConnectClientConfiguration = InitialiseClientConfiguration();


            yield return $"Loaded {model.Content.Count()} content items";

            yield return "Connected to xConnect";

            var days = (model.EndDate - model.StartDate).Days;
            var approximateTotalInteractions = days * model.Traffic.InteractionsPerDay;

            yield return $"Starting Generation of approximately {approximateTotalInteractions} interactions";

            var dates = new List<DateTime>();

            for (var i = 0; i < days; i++)
            {
                dates.Add(model.StartDate.AddDays(i));
            }

            var existingContacts = new List<Guid?>();
            var randomGenerator = new Random((int)DateTime.Now.Ticks);

            foreach (var date in dates)
            {
                yield return $"Generating traffic for {date}";

                var batchSize = _configuration.BatchSize;

                var interactionsForThisDay = CalculateInteractionsForThisDay(model, date);

                IList<Tuple<KnownPathModel, int>> batches = new List<Tuple<KnownPathModel, int>>();


                foreach (var knownPath in model.Traffic.KnownPaths)
                {
                    var remainingInteractionsForPath = (int) (knownPath.PercentageOfTraffic * interactionsForThisDay);

                    while (remainingInteractionsForPath > 0)
                    {
                        var curentBatchSize = remainingInteractionsForPath > batchSize ? batchSize : remainingInteractionsForPath;
                        batches.Add(Tuple.Create(knownPath, curentBatchSize));

                        remainingInteractionsForPath = remainingInteractionsForPath - batchSize;
                    }
                }

                var createdContacts = batches.AsParallel().SelectMany(tuple =>
                    GenerateBatch(xConnectClientConfiguration, tuple.Item2, date, model, existingContacts,
                        randomGenerator, tuple.Item1)).Distinct().ToArray();

                existingContacts.AddRange(createdContacts);

                PruneExistingContacts(existingContacts);

                yield return $"Finished generating traffic for {date}";
            }

            yield return "Done";
        }

        private static int CalculateInteractionsForThisDay(GenerationModel model, DateTime date)
        {
            var numberOfInteractions =
                model.Traffic.InteractionsPerDay * model.Traffic.DayBias.GetBias(date.DayOfWeek) * model.Traffic.MonthBias.GetBias(date.Month);
            return (int)numberOfInteractions;
        }

        private void PruneExistingContacts(List<Guid?> existingContacts)
        {
            var countOfContacts = existingContacts.Count;
            existingContacts.RemoveRange(0, countOfContacts);
        }

        private static IEnumerable<Guid?> GenerateBatch(XConnectClientConfiguration xConnectClientConfiguration,
            int numberOfInteractions,
            DateTime date, GenerationModel model, List<Guid?> existingContacts, Random random, KnownPathModel knownPath)
        {
            var createdContacts = new List<Contact>();

            using (var client = new XConnectClient(xConnectClientConfiguration))
            {
                for (var interactionNumber = 0; interactionNumber < numberOfInteractions; interactionNumber++)
                {
                    var contact = ContactGenerator.GenerateContact(model.Audience, existingContacts, client, random);
                    createdContacts.Add(contact);

                    var interaction = InteractionGenerator.GenerateInteraction(date, contact, model, client, knownPath);




                }

                client.Submit();
            }


            return createdContacts.Select(contact => contact.Id);
        }

        private XConnectClientConfiguration InitialiseClientConfiguration()
        {
            var requestModifiers = new List<IHttpClientHandlerModifier>();
            var clientModifiers = new List<IHttpClientModifier>();

            var certificateModifier = GetCertificateModifier();
            if (certificateModifier != null)
                requestModifiers.Add(certificateModifier);

            var timeoutClientModifier = GetTimeOutModifier();
            if (timeoutClientModifier != null)
                clientModifiers.Add(timeoutClientModifier);

            var collectionClient =
                new CollectionWebApiClient(_configuration.XConnectCollectionEndPoint, clientModifiers,
                    requestModifiers);
            var searchClient =
                new SearchWebApiClient(_configuration.XConnectSearchEndPoint, clientModifiers, requestModifiers);
            var configurationClient = new ConfigurationWebApiClient(_configuration.XConnectConfigurationEndPoint,
                clientModifiers, requestModifiers);

            var xConnectClientConfiguration = new XConnectClientConfiguration(
                new XdbRuntimeModel(CollectionModel.Model), collectionClient, searchClient, configurationClient, true);

            xConnectClientConfiguration.Initialize();
            return xConnectClientConfiguration;
        }

        private TimeoutHttpClientModifier GetTimeOutModifier()
        {
            return !TimeSpan.TryParse(_configuration.XConnectTimeout, out var timeSpan)
                ? null
                : new TimeoutHttpClientModifier(timeSpan);
        }

        private IHttpClientHandlerModifier GetCertificateModifier()
        {
            if (string.IsNullOrEmpty(_configuration.XConnectCertificateName)) return null;
            var options =
                CertificateHttpClientHandlerModifierOptions.Parse(_configuration.XConnectCertificateName);

            return new CertificateHttpClientHandlerModifier(options);
        }
    }
}