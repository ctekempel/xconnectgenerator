﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Collection.Model;
using TrafficGenerator.Content.Model;
using TrafficGenerator.Generation.Model;

namespace TrafficGenerator.Generation
{
    public class InteractionGenerator
    {
        public static Interaction GenerateInteraction(DateTime date, Contact contact, GenerationModel model,
            XConnectClient client, KnownPathModel knownPath)
        {
            var channelId = knownPath.ChannelId;
            var interaction = new Interaction(contact, InteractionInitiator.Contact, channelId, model.Audience.UserAgents.PickRandom());

            var events = GenerateContentImpressions(date, model, interaction, knownPath);

            foreach (var contentImpressionEvent in events)
            {
                interaction.Events.Add(contentImpressionEvent);
            }

            PopulateWebVisitFacet(model, client, knownPath, interaction);

            PopulateIpInfoFacet(model, client, interaction);

            client.AddInteraction(interaction);

            return interaction;
        }

        private static void PopulateIpInfoFacet(GenerationModel model, XConnectClient client, Interaction interaction)
        {
            var location = model.Audience.Locations.PickRandom();

            var ipInfoFacet1 = new IpInfo(location.IpAddress)
            {
                Country = location.Country,
                Region = location.Region,
                City = location.City
            };
            var ipInfoFacet = ipInfoFacet1;

            client.SetFacet(interaction, IpInfo.DefaultFacetKey, ipInfoFacet);
        }

        private static void PopulateWebVisitFacet(GenerationModel model, XConnectClient client, KnownPathModel knownPath,
            Interaction interaction)
        {
            var webVisitFacet = new WebVisit
            {
                SiteName = model.Traffic.SiteName,
                Language = knownPath.Language,
                Referrer = knownPath.Referrer
            };
            client.SetFacet(interaction, WebVisit.DefaultFacetKey, webVisitFacet);
        }

        private static List<Event> GenerateContentImpressions(DateTime date, GenerationModel model,
            Interaction interaction, KnownPathModel knownPath)
        {
            var events = new List<Event>();

            if (knownPath.CampaignId.HasValue) interaction.CampaignId = knownPath.CampaignId.Value;

            foreach (var pathNode in knownPath.Path)
            {
                var timestamp = date.AddMinutes(400);

                var item = GetItem(model, pathNode);

                var pageViewEvent = GetPageViewEvent(timestamp, item, pathNode);
                events.Add(pageViewEvent);

                AddSearchKeywords(timestamp, pageViewEvent, events, pathNode);
                AddGoals(model, item, timestamp, pageViewEvent, events);
                AddOutcomes(timestamp, pageViewEvent, events, pathNode);

                AddCampaigns(item, events, timestamp, pageViewEvent);
            }

            return events;
        }

        private static ContentItem GetItem(GenerationModel model, IItemImpression pathNode)
        {
            ContentItem item;

            if (pathNode is ContentImpression)
                item = model.Content[$"{model.Traffic.ItemRoot}{pathNode.Path}"];
            else if (pathNode is MediaImpression)
                item = model.Media[$"{model.Traffic.MediaRoot}{pathNode.Path}"];
            else
                throw new ArgumentException(
                    $"Item {pathNode.Path} is not a valid content type");

            if (item == null)
                throw new ArgumentException(
                    $"Item {pathNode.Path} could not be found, please ensure it exists under either the item root or media root");

            return item;
        }

        private static PageViewEvent GetPageViewEvent(DateTime timestamp, ContentItem item, IItemImpression pathNode)
        {
            var pageViewEvent = new PageViewEvent(timestamp, item.Id, item.Version, item.Language)
            {
                Url = pathNode.Path
            };
            return pageViewEvent;
        }

        private static void AddCampaigns(ContentItem item, List<Event> events, DateTime timestamp,
            PageViewEvent pageViewEvent)
        {
            if (item.Tracking.Campaigns.Any())
                foreach (var campaign in item.Tracking.Campaigns)
                    events.Add(new CampaignEvent(campaign.Id, timestamp) {ParentEventId = pageViewEvent.Id});
        }

        private static void AddGoals(GenerationModel model, ContentItem item, DateTime timestamp,
            PageViewEvent pageViewEvent,
            List<Event> events)
        {
            if (item.Tracking.Events.Any())
                foreach (var trackingEvent in item.Tracking.Events)
                {
                    var eventItem = model.Events[trackingEvent.Id];
                    if (eventItem != null)
                    {
                        Event @event;

                        if (eventItem.TemplateName == "Goal")
                            @event = new Goal(eventItem.Id, timestamp);
                        else if (eventItem.Name == "Download")
                            @event = new DownloadEvent(timestamp, item.Id);
                        else
                            throw new ArgumentException($"Unkown event type {eventItem.Path}");

                        @event.ParentEventId = pageViewEvent.Id;
                        @event.EngagementValue = eventItem.EngagementValue;

                        events.Add(@event);
                    }
                }
        }

        private static void AddOutcomes(DateTime timestamp,
            PageViewEvent pageViewEvent,
            List<Event> events, IItemImpression pathNode)
        {
            if (pathNode.Outcome != null)
            {
                var outcome = new Outcome(pathNode.Outcome.OutcomeId, timestamp, pathNode.Outcome.CurrencyCode,
                    pathNode.Outcome.MonetaryValue);
                outcome.ParentEventId = pageViewEvent.Id;
                events.Add(outcome);
            }
        }

        private static void AddSearchKeywords(DateTime timestamp, PageViewEvent pageViewEvent, List<Event> events, IItemImpression pathNode)
        {
            if (!string.IsNullOrEmpty(pathNode.SearchPhrase))
            {
                var searchEvent = new SearchEvent(timestamp){Keywords = pathNode.SearchPhrase,ParentEventId = pageViewEvent.Id};
                events.Add(searchEvent);
            }
        }
    }
}