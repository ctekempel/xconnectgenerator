﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using TrafficGenerator.Generation.Model;

namespace TrafficGenerator.Generation
{
    public class ContactGenerator
    {
        public static Contact GenerateContact(AudienceModel audienceModel, List<Guid?> existingContacts,
            XConnectClient client, Random random)
        {
            Contact contact;

            bool newContact = (random.Next(0, 10000) < (audienceModel.PercentageNewContacts * 100));
            if (newContact || !existingContacts.Any())
            {
                bool knownContact = (random.Next(0, 10000) < (audienceModel.PercentageKnownContacts * 100));

                if (knownContact)
                {
                    contact = new Contact(new ContactIdentifier("xConnectGenerator", Guid.NewGuid().ToString(),
                        ContactIdentifierType.Known));
                }
                else
                {
                    contact = new Contact();

                }
                client.AddContact(contact);

                return contact;

            }
            else
            {
                var existingContact = existingContacts[random.Next(0, existingContacts.Count - 1)];
                var reference = new ContactReference(existingContact.Value);
                contact = client.GetAsync(reference, new ContactExpandOptions()).Result;
            }
            return contact;
        }
    }
}